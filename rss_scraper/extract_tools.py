import json


def extract(feed) -> list:
    '''take parsed feed as argument
    return list of dict(title, summary, link, published)
    '''
    return [dict(title=e.title,
                 summary=e.summary,
                 link=e.link,
                 published=e.published) for e in feed.entries]


def get_id(url) -> str:
    '''take article url
    return article id
    '''
    id = url.split('?')[0].split('/')[-1].split('-')[-1]
    try:
        int(id)
    except:
        raise ValueError('wrong id, integer expected, got:', id)

    return id


def write_json(file_name, data: list) -> None:
    with open(file_name, 'w') as f:
        json.dump(data, f)


if __name__ == '__main__':
    lst = [
        'https://www.bbc.co.uk/news/asia-shmazia-58160547?at_medium=RSS&at_campaign=KARANGA',
        'https://www.bbc.co.uk/sport/football/58859225?at_medium=RSS&at_campaign=KARANGA'
    ]
    for e in lst:
        print(get_id(e))
