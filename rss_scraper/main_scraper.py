import feedparser
import threading
from pathlib import Path
import shutil
from scrap_img_link import scrap_img_link
from get_write_image import write_img
from extract_tools import get_id, extract, write_json

'''PIPELINE:
1 --> extract data from rss feed
2 --> iterate over data, scrap first img link and download the picture to 'output/images/' folder,
      add local image link to the entry
3 --> write json to 'output/'
'''

URL = 'http://feeds.bbci.co.uk/news/world/rss.xml'
FOLDER = Path('..', 'static', 'output')
IMG_FOLDER = 'images'
WAIT_TIME = 900.0


def download_image(entry: dict) -> dict:
    '''scrap img links
    download pictures
    add local files links to the entry    
    '''
    article_url = entry['link']
    article_id = get_id(article_url)
    entry['id'] = article_id
    print('scraping article: ', article_url)
    entry['img_source'], entry['copyright'] = scrap_img_link(article_url)

    # download picture, add file link to entry
    if entry['img_source']:
        file_name = "{0}{1}".format(article_id, entry['img_source'][-4:])
        file_path = Path(FOLDER, IMG_FOLDER, file_name)
        write_img(entry['img_source'], file_path)
        entry['img_file'] = str(file_path)
    else:
        entry['img_file'] = None


def main():
    feed = feedparser.parse(URL)
    data = extract(feed)
    print('extracted: ', len(data))

    # create output folder
    FOLDER.mkdir(exist_ok=True)

    # remove outdated image files
    i_path = Path(FOLDER, IMG_FOLDER)
    if i_path.exists():
        shutil.rmtree(i_path)
    i_path.mkdir()

    # iterate over data and scratch images
    for entry in data:
        download_image(entry)

    write_json(Path(FOLDER, 'feed.json'), data)
    print('json length: ', len(data))


class Scraper(threading.Thread):
    def run(self):
        c = 0
        while True:
            print('start scraping loop # ', c)

            main()

            c += 1
            print('waiting for the next loop', '\n')
            event.wait(WAIT_TIME)


event = threading.Event()


if __name__ == '__main__':
    Scraper().start()
