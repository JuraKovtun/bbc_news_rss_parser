import requests


def write_img(URL, name):
    '''take image URL and file name as arguments
    send request, save image file
    '''
    img = requests.get(URL).content
    with open(name, 'wb') as f:
        f.write(img)


if __name__ == '__main__':
    URL = 'https://ichef.bbci.co.uk/news/976/cpsprodpb/8AA5/production/_118539453_8c81b80f-ddcd-48df-b99a-5f788478a055.jpg'
    write_img(URL, 'ss.jpg')
