import requests
from bs4 import BeautifulSoup


def scrap_img_link(URL: str) -> list:
    '''take article URL
    return image source URL, copyright
    if article has no images
    (in case there is only video, rendered via javascript) we skip it
    '''
    # with open('test.html') as f:
    #     soup = BeautifulSoup(f, 'html.parser')
    page = requests.get(URL).content
    soup = BeautifulSoup(page, 'html.parser')

    if 'news' in URL:
        container = soup.article.find(attrs={'data-component': 'image-block'})
        if container:
            img_src = container.find('img').get_attribute_list('src')[0]
            cp_right = container.find(
                'span', string='Image source, ')
            if cp_right:
                cp_right = cp_right.next_sibling
        else:
            # if there is no image we skip the article
            img_src, cp_right = None, None
    elif 'sport' in URL:
        img_src = soup.article.find('img').get_attribute_list('src')[0]
        cp_right = None
    else:
        raise ValueError(
            'not able to extract image source from the article: ', URL)

    return img_src, cp_right


if __name__ == '__main__':
    # URL = 'https://www.bbc.co.uk/sport/boxing/58862448?at_medium=RSS&at_campaign=KARANGA'
    URL = 'https://www.bbc.com/news/world-australia-59046032'
    print(scrap_img_link(URL))
