const container_big = document.getElementsByClassName('big_inner')[0];
const container_small = document.getElementsByClassName('small_inner')[0];


function create_big_news(image, header, text, copyright) {
    let e = document.createElement('div');
    e.className = 'big_news';
    e.innerHTML = `<span class="big_picture" style="--copyright-var:'${copyright}'; background-image:url(${image})"></span>
                    <h1 class="big_header">${header}</h1>
                    <p class="big_text">${text}</p>`;
    return e;
}


function create_small_news(image, header, text, copyright) {
    let e = document.createElement('div');
    e.className = 'small_news';
    e.innerHTML = `<span class="small_picture" style="--copyright-var:'${copyright}'; background-image:url(${image})"></span>
                    <h1 class="small_header">${header}</h1>
                    <p class="small_text">${text}</p>`;
    return e;
}



function render(data) {
    // create big_news elements
    for (let news of data) {
        let e = create_big_news(news.img_file, news.title, news.summary, news.copyright);
        container_big.append(e)
    }

    // create small_news elements, starting with index 1
    for (let i = 0; i < data.length; i++) {
        let news = data[(i + 1) % data.length];
        let e = create_small_news(news.img_file, news.title, news.summary, news.copyright);
        container_small.append(e);
    }
}




// animate.small();
// setInterval(() => {
//     animate.small();
// }, 2500)


// function animate(big_news, small_news) {
//     small_animation(small_news);
//     // big_animation(big_news);
//     let c = 0;

//     let interval_id = setInterval(() => {
//         c += 1;
//         console.log('leak', c)
//         small_animation(small_news);
//         // big_animation(big_news);
//     }, loop_time)

//     return interval_id
// }






