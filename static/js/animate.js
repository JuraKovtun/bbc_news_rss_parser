const b = 2; // number of big visible elements
const s = 5; // number of small visible elements
const motion_time_big = 1200;
const motion_time_small = 1200;
const offset_time_big = 100;
const offset_time_small = 60;
const animation_time_small = motion_time_small + offset_time_small * (s - 1);
const loop_time = 10000;


const motion = {
    timeouts: {
        big: [],
        small: []
    },
    big(container) {
        // container is DOM element
        let children = container.children;

        // clear existimg timeouts
        let ids = this.timeouts['big'];
        ids.map(id => clearTimeout(id));

        // iterate over visible elements and set small_motion css class
        let visible = Array.from(children).slice(0, b);
        ids = visible.map((e, i) => setTimeout(() => {
            e.classList.add('big_motion')
        }, offset_time_big * i)
        )

        // remove animation css class, move the first element to the last position
        ids = [
            ...ids,
            ...visible.map(e => setTimeout(() => {
                e.classList.remove('big_motion')
            }, motion_time_big + offset_time_big * (b - 1))),
            setTimeout(() => {
                container.append(container.removeChild(container.firstChild));
            }, motion_time_big + offset_time_big * (b - 1))
        ];
    },
    small(container) {
        // container is DOM element
        let children = container.children;

        // clear existimg timeouts
        let ids = this.timeouts['small'];
        ids.map(id => clearTimeout(id));

        // iterate over visible elements and set small_motion css class
        let visible = Array.from(children).slice(0, s);
        ids = visible.map((e, i) => setTimeout(() => {
            e.classList.add('small_motion')
        }, offset_time_small * i)
        )

        // remove animation css class, move the first element to the last position
        ids = [
            ...ids,
            ...visible.map(e => setTimeout(() => {
                e.classList.remove('small_motion')
            }, motion_time_small + offset_time_small * (s - 1))),
            setTimeout(() => {
                container.append(container.removeChild(container.firstChild));
            }, motion_time_small + offset_time_small * (s - 1))
        ];
    }
}



function animate(c_big, c_small) {
    motion.big(c_big);
    motion.small(c_small);

    let id = setInterval(() => {
        motion.big(c_big);
        motion.small(c_small);
    }, loop_time)

    return id
}


