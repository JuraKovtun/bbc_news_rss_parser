
const RELOAD_INTERVAL = 60000;
const c_big = document.querySelector('.big_inner');
const c_small = document.querySelector('.small_inner');
let interval_id = undefined;

const main = () => {
    let data = test_data;

    // filter out records without image
    data = data.filter(news => news.img_file);

    // remove existing content
    c_big.innerHTML = '';
    c_small.innerHTML = '';

    // render new content
    render(data);

    clearInterval(interval_id);
    interval_id = animate(c_big, c_small);
    console.log('data length:', data.length, 'rendered elements:', c_small.children.length)
}

main();

setInterval(() => {
    main();
}, RELOAD_INTERVAL)



