const requestURL = 'http://34.67.63.196:5000/feed';
// const requestURL = 'http://192.168.133.128:5000/feed';
const RELOAD_INTERVAL = 60000;
const c_big = document.querySelector('.big_inner');
const c_small = document.querySelector('.small_inner');
let interval_id = undefined;

let request = new XMLHttpRequest();
request.open('GET', requestURL);
request.responseType = 'json';
request.onload = () => {
    let data = request.response;

    // filter out records without image
    data = data.filter(news => news.img_file);

    // remove existing content
    c_big.innerHTML = '';
    c_small.innerHTML = '';

    // render new content
    render(data);

    clearInterval(interval_id);
    interval_id = animate(c_big, c_small);
    console.log('data length:', data.length, 'rendered elements:', c_small.children.length)
}
request.send();

setInterval(() => {
    request.open('GET', requestURL);
    request.send();
}, RELOAD_INTERVAL)



