from flask import Flask, send_from_directory
app = Flask(__name__)


@app.route('/')
def send_index():
    return send_from_directory('static', 'index.html')


@app.route('/test')
def send_test():
    return send_from_directory('static', 'test.html')


@app.route('/feed')
def send_feed():
    return send_from_directory('static', 'output/feed.json')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
